<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutScene</name>
    <message>
        <location filename="../qml/scenes/AboutScene.qml" line="22"/>
        <source>ABOUT_GAME</source>
        <translation>&lt;b&gt;Defend The Eagle &lt;/b&gt; wurde im Rahmen der &lt;i&gt; Felgo Challenge &lt;/i&gt; für Łukasz Kosiński entwickelt</translation>
    </message>
    <message>
        <location filename="../qml/scenes/AboutScene.qml" line="42"/>
        <source>VISIT_FELGO</source>
        <oldsource>Visit Felgo</oldsource>
        <translation>Besuche Felgo</translation>
    </message>
    <message>
        <location filename="../qml/scenes/AboutScene.qml" line="64"/>
        <source>VISIT_REPO</source>
        <oldsource>Visit repo</oldsource>
        <translation>Besuche repo</translation>
    </message>
    <message>
        <location filename="../qml/scenes/AboutScene.qml" line="88"/>
        <source>BACK</source>
        <oldsource>Back</oldsource>
        <translation>Zurück</translation>
    </message>
</context>
<context>
    <name>GameScene</name>
    <message>
        <location filename="../qml/scenes/GameScene.qml" line="162"/>
        <source>GAME_OVER</source>
        <translation>Amateur</translation>
    </message>
</context>
<context>
    <name>MenuScene</name>
    <message>
        <location filename="../qml/scenes/MenuScene.qml" line="46"/>
        <source>START_GAME</source>
        <oldsource>Start Game</oldsource>
        <translation>Spiel</translation>
    </message>
    <message>
        <location filename="../qml/scenes/MenuScene.qml" line="68"/>
        <source>SETTINGS</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/scenes/MenuScene.qml" line="92"/>
        <source>ABOUT</source>
        <oldsource>About</oldsource>
        <translation>Über</translation>
    </message>
</context>
<context>
    <name>SettingsScene</name>
    <message>
        <location filename="../qml/scenes/SettingsScene.qml" line="29"/>
        <source>LANGUAGE</source>
        <oldsource>Language</oldsource>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../qml/scenes/SettingsScene.qml" line="72"/>
        <source>MUSIC</source>
        <oldsource>Music</oldsource>
        <translation>Musik</translation>
    </message>
    <message>
        <location filename="../qml/scenes/SettingsScene.qml" line="96"/>
        <source>BACK</source>
        <oldsource>Back</oldsource>
        <translation>Zurück</translation>
    </message>
</context>
<context>
    <name>TranslationManager</name>
    <message>
        <location filename="../TranslationManager.cpp" line="68"/>
        <source>TARGET_LANGUAGE</source>
        <translation>Deutsch</translation>
    </message>
</context>
</TS>
