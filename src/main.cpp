#include <FelgoApplication>
#include <QApplication>

#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "TranslationManager.h"

int main(int argc, char *argv[])
{

    QApplication app(argc, argv);

    FelgoApplication felgo;

    QQmlApplicationEngine engine;
    felgo.initialize(&engine);

    felgo.setLicenseKey(PRODUCT_LICENSE_KEY);

    TranslationManager translationManager(&engine);
    engine.rootContext()->setContextProperty("TranslationManager", &translationManager); // exposing translationManager to Qml

    felgo.setMainQmlFileName(QStringLiteral("qml/DefendTheEagle.qml"));
    engine.load(QUrl(felgo.mainQmlFileName()));

    return app.exec();
}
