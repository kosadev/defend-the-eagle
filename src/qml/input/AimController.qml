import Felgo 3.0
import QtGraphicalEffects 1.0
import QtQuick 2.12

// AimController represents button which player hits to fire
Item {
    id: aimController

    signal aimTriggered
    property int size: 50

    height: size
    width: size

    Image {
        id: aimImg
        anchors.fill: parent
        source: "../../assets/img/target.png"

        smooth: true
        visible: false
    }


    Rectangle {
        id: aimSource
        anchors.fill: aimController
        color: "goldenrod"
        visible: false
    }

    OpacityMask {
        id: aimControllerMask

        anchors.fill: aimController

        maskSource: aimImg
        source: aimSource
    }

    MouseArea {
      anchors.fill: parent
      onPressed: {
        aimController.aimTriggered()
      }
    }
}
