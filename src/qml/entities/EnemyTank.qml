import Felgo 3.0
import QtQuick 2.0
import "../input"

// EnemyTank is an item to convenient extend behavior of normal tank. There entities works without user's control
Tank {
    id: enemyTank
    entityType: "enemyTank"

    // tank's speed. Enemy tanks are slower
    property real forwardForce: 150 * world.pixelsPerMeter

    tankImageSource: "../../assets/img/enemy-tank.png"

    // direction change will be performed when current tank hits the Wall or Obstruction
    onWallHitted: {
        changeDirection()
    }

    // also move enemy just after it's completed
    Component.onCompleted: {
        changeDirection()
    }

    // timer is used to triggered shot from EnemyTank
    Timer {
        interval: 2000; repeat: true; // every 2 seconds
        running: true // works from beginning
        onTriggered: {
            fire();
        }
    }

    // timer is used to make enemy's beavior less predictable
    Timer {
        interval: 3500; repeat: true; // every 3.5 seconds
        running: true // works from beginning
        onTriggered: {
            changeDirection()
        }
    }

    // new random direction is going to be passed as artificial user input
    function changeDirection() {
        var literalsAmount = 5
        var randomAction = Math.floor(Math.random() * literalsAmount)

        // to avoid additional shots we will increment our action if it's Fire action
        if(randomAction === Input.InputAction.Fire) {
            randomAction = 1; // some other value
        }

        // if randomized direction is same as actual then try once again
        if(isSameDirection(randomAction)) {
            changeDirection()
        } else {
            handleUserInput(randomAction)
        }
    }

    // function to check if passed direction is same as actual
    // function basses on actual axises
    function isSameDirection(direction) {

        var actualDirection = 0

        var xAxis = twoAxisController.xAxis
        var yAxis = twoAxisController.yAxis

        // checks axises for their direction and set actualDirection accordingly
        if(xAxis == 1) {
            actualDirection = Input.InputAction.MoveRight
        }
        if(xAxis == -1) {
            actualDirection = Input.InputAction.MoveLeft
        }
        if(yAxis == 1) {
            actualDirection = Input.InputAction.MoveDown
        }
        if(yAxis == -1) {
            actualDirection = Input.InputAction.MoveUp
        }

        return actualDirection === direction
    }

}
