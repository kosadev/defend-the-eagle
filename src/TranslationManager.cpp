#include "TranslationManager.h"

#include <QCoreApplication>
#include <QDebug>
#include <QDirIterator>
#include <QTranslator>

#define CONTEXT "TranslatorFactoryContext"

void TranslationManager::switchToLanguage(const QString &languageCode)
{

    // iterate over installed translators to delete them. Helpful in applications which
    // need more long maintance
    for(auto translator : _installedTranslators) {
        qApp->removeTranslator(translator);
        delete translator;
    }
    _installedTranslators.clear();

    QString code = languageCode;

    // let's try to load OS specified language
    if(code == "system") {
        QLocale l = QLocale::system();
        code = l.name();
    }

    if(!isTranslationAvailable(code)) {
        code = _defaultLanguage;
    }

    QTranslator *translator = new QTranslator();
    const QString path = ":/translations/app/"+code+".qm";
    translator->load(path);
    this->_installedTranslators << translator;
    qApp->installTranslator(translator);

    _currentLanguage = code;

    _engine->retranslate();
}

TranslationManager::TranslationManager(QQmlEngine *engine, QObject *parent) :
    QObject (parent), _engine(engine)
{
}

QMap<QString, QString> TranslationManager::avaliableTranslations() const
{

    QDirIterator it(":/translations/app");
    QMap<QString, int> counter;
    bool processed = false;
    QMap<QString, QString> translations;
    if(processed) {
        return translations;
    }

    processed = true;
    while (it.hasNext()) {
        QTranslator trans;
        const QString qmfile = it.next();
        QFileInfo fi(qmfile);
        trans.load(qmfile);
        // we expect that TARGET_LANGUAGE is translated
        QString label = trans.translate("TranslationManager", "TARGET_LANGUAGE");
        if(label == "") {
            label = "Unknown Language ("+fi.baseName()+")";
        }
        if(counter.contains(label)) {
            int value = counter[label] + 1;
            counter[label] = value;
            label += QString(" %1").arg(value);
        }
        else {
            counter[label] = 0;
        }
        translations[label] = fi.baseName();
    }
    return translations;
}

bool TranslationManager::isTranslationAvailable(const QString &languageCode) const
{
    QMap<QString, QString> translations = this->avaliableTranslations();

    for(auto key : translations.keys()) {
        if(translations.value(key) == languageCode) {
            return true;
        }
    }

    return false;
}

TranslationManager::~TranslationManager()
{
}
