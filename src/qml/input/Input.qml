import Felgo 3.0
import QtQuick 2.0

Item {

    // InputAction enumeration type allows to keep code more clean
    // and to pass explicity specify passed parameters instead of using e.g. strings
    enum InputAction {
        Fire,
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,        
        Stop
    }
}
