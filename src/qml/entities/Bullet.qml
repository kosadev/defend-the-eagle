import Felgo 3.0
import QtQuick 2.12

EntityBase {

    id: bullet
    entityType: "bullet"

    readonly property int speed: 6000
    // targetPoint property allows to pass direction of bullet from outside Bullet.qml
    property point targetPoint: Qt.point(0, -1)
    property int size: 10

    BoxCollider {
        id: boxCollider

        width: size
        height: size
        anchors.centerIn: parent

        density: 0.1
        // this parameter is for small and fast objects. Just like bullets
        body.bullet: true

        // always one parametr ( x or y ) of target is set to 0 while the second one is set to 1 or -1
        // it allows to point perpendicular direction for the bullet
        force: Qt.point(targetPoint.x * speed, targetPoint.y * speed)

        // bullet disappear after every contact
        fixture.onBeginContact: {
            removeEntity()
        }
    }

    Image {
        anchors.centerIn: parent
        width: boxCollider.width
        height: boxCollider.height
        source: "../../assets/img/bullet.png"
    }
}
