import Felgo 3.0
import QtQuick 2.0

// Obstraction is solid element on arena with bricks texture
EntityBase {
    entityType: "obstruction"

    Image {
        anchors.fill: collider
        source: "../../assets/img/brick.jpg"

        // to scale down source
        sourceSize.height: 128
        sourceSize.width: 128

        // to behaves like texture
        fillMode: Image.Tile
    }

    BoxCollider {
        id: collider
        anchors.fill: parent

        // walls should be solid. Nothing passes
        bodyType: Body.Static
    }
}
