import Felgo 3.0
import QtQuick 2.0
import "../input"

Scene {

    id: gameScene

    readonly property int sideBarWidth: (gameScene.width - gameScene.height) / 2
    property alias timerGenerator: timerGenerator

    // for creating and destroying entities at runtime. Creatable entities in the case of this
    // game, are Bullet and EnemyTank entities
    EntityManager {
        id: entityManager
        entityContainer: arena
    }

    // timer is used to generate new enemies
    Timer {
        id: timerGenerator
        interval: 5000; repeat: true; // every 5 seconds
        onTriggered: {
            generateEnemy();
        }
    }

    // use a physics world because we need collision detection
    PhysicsWorld {
        id: world
    }

    // stage where game's action is being performed. Holds map and entities definitions.
    Arena {
        id: arena

        anchors.horizontalCenter: parent.horizontalCenter
        size: gameScene.height

        // if player's tank was destroyed that it's end of the game
        playerTank.onTankDestroyed: {
            gameOver()
        }
    }

    // moveController item represents four buttons for user to control tank.
    // After one of them is clicked, handleInput() method from Arena is called.
    MoveController {
        id: moveController

        buttonsColor: "goldenrod"
        size: sideBarWidth * 0.85

        anchors.bottom: parent.bottom
        anchors.bottomMargin: sideBarWidth * 0.1

        anchors.left: parent.left
        anchors.leftMargin: (sideBarWidth - size) / 2

        onInputHandled: {
            arena.handleInput(action)
        }
    }

    // aimController item which draws button with aim. After controller is triggered
    // Fire InputAction is sent to Arena to be finally passed to player's tank
    AimController {
        id: aimController

        size: sideBarWidth * 0.85

        anchors.bottom: parent.bottom
        anchors.bottomMargin: sideBarWidth * 0.1

        anchors.right: parent.right
        anchors.rightMargin: (sideBarWidth - size) / 2

        onAimTriggered: {
            // Arena item contains handleInput() function where InputAction is passed further
            arena.handleInput(Input.InputAction.Fire)
        }
    }



    // background of current scene
    Image {
        id: gameSceneBackground
        anchors.fill: parent
        source: "../../assets/img/metal.png"

        // to repeat texture
        fillMode: Image.Tile

        // to scale down texture
        sourceSize.width: 64
        sourceSize.height: 64

        // to hide background below other elements created earlier
        z: -1

        // to make background a bit darker we set this rectangle's opacity to 0.5
        Rectangle {
            anchors.fill: parent
            color: "black"
            opacity: 0.5
        }
    }



    // we want also to handle keys input so now we'll connect Arena.handleInput()
    // function to be called on WASD Keys events
    // gameScene needs to be on focus to handle keys events
    focus: true
    // to pass movement and fire events
    Keys.onPressed: {
        if (event.key === Qt.Key_W) {
            arena.handleInput(Input.InputAction.MoveUp)
        }
        if (event.key === Qt.Key_S) {
            arena.handleInput(Input.InputAction.MoveDown)
        }
        if (event.key === Qt.Key_A) {
            arena.handleInput(Input.InputAction.MoveLeft)
        }
        if (event.key === Qt.Key_D) {
            arena.handleInput(Input.InputAction.MoveRight)
        }
        if (event.key === Qt.Key_Enter) {
            arena.handleInput(Input.InputAction.Fire)
        }
        if (event.key === Qt.Key_Space) {
            arena.handleInput(Input.InputAction.Fire)
        }
    }

    // to stop movement in selected direction after key is released
    Keys.onReleased: {
        if (event.key === Qt.Key_W ||
                event.key === Qt.Key_S ||
                event.key === Qt.Key_A ||
                event.key === Qt.Key_D)
        {
            arena.handleInput(Input.InputAction.StopRight)
        }
    }

    // message box which appears if player loses
    Rectangle {
        id: gameOverBox

        anchors.fill: parent
        color: "black"

        enabled: false
        visible: false

        Text {
            anchors.centerIn: parent

            // game over message
            text: qsTr("GAME_OVER")
            color: "white"

            font.pixelSize: parent.height / 5
            font.bold: true
        }

        // click on every place after game over causes application quit
        MouseArea {
            anchors.fill: parent
            onClicked: {
                Qt.quit()
            }
        }

    }

    // used to spawn enemy tank on Arena
    function generateEnemy() {

        // list approximate coordinates of arena's corners
        var arenaCornersPoints = [ Qt.point(arena.x + 10, 10),
                                  Qt.point(arena.x + 10, arena.height - 10),
                                  Qt.point(arena.x + arena.width - 10, 10),
                                  Qt.point(arena.x + arena.width - 10, arena.height - 10)]

        // randomize one bazing on list of available places to put enemies
        var randomCorner = arenaCornersPoints[Math.floor(Math.random() * arenaCornersPoints.length)]

        // maps point to Arena. As a result we get matched coordinates on Arena
        var mappedCoordinates = mapToItem(arena, randomCorner.x, randomCorner.y)

        // new enemies are positioned with these coordinates and have size defined in Arena
        entityManager.createEntityFromUrlWithProperties(
                    Qt.resolvedUrl("../entities/EnemyTank.qml"),
                    { "x": mappedCoordinates.x,
                        "y": mappedCoordinates.y,
                        "size": arena.mobSize})
    }

    // function to inform user that he lost
    function gameOver() {
        gameOverBox.visible = true
        gameOverBox.enabled = true
    }
}
