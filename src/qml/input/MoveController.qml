import Felgo 3.0
import QtQuick 2.12

// MoveController is short for pad-like buttons to control player's tank.
// On PC it's replaced by Keys
Item {

    id: moveController

    property int size: 50
    property int buttonsSize: size / 3
    property color buttonsColor: "white"

    // inputHandled signal is emitted with adjusted Input.InputAction value
    // after press or release of controls buttons
    signal inputHandled(int action)

    height: size
    width: size

    Rectangle {
        id: upBotton

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top

        height: buttonsSize
        width: buttonsSize

        color: buttonsColor
        radius: height/2

        MouseArea {
            anchors.fill: parent
            onPressed: moveController.inputHandled(Input.InputAction.MoveUp)
            onReleased: moveController.inputHandled(Input.InputAction.Stop)
        }
    }

    Rectangle {
        id: downBotton

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom

        height: buttonsSize
        width: buttonsSize

        color: buttonsColor
        radius: height/2

        MouseArea {
            anchors.fill: parent
            onPressed: moveController.inputHandled(Input.InputAction.MoveDown)
            onReleased: moveController.inputHandled(Input.InputAction.Stop)
        }
    }

    Rectangle {
        id: leftBotton

        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left

        height: buttonsSize
        width: buttonsSize

        color: buttonsColor
        radius: height/2

        MouseArea {
            anchors.fill: parent
            onPressed: moveController.inputHandled(Input.InputAction.MoveLeft)
            onReleased: moveController.inputHandled(Input.InputAction.Stop)
        }
    }

    Rectangle {
        id: rightBotton

        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right

        height: buttonsSize
        width: buttonsSize

        color: buttonsColor
        radius: height/2

        MouseArea {
            anchors.fill: parent
            onPressed: moveController.inputHandled(Input.InputAction.MoveRight)
            onReleased: moveController.inputHandled(Input.InputAction.Stop)
        }
    }
}
