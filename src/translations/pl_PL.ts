<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>AboutScene</name>
    <message>
        <location filename="../qml/scenes/AboutScene.qml" line="22"/>
        <source>ABOUT_GAME</source>
        <translation>&lt;b&gt;Defend The Eagle&lt;/b&gt; powstała na potrzeby wyzwanie &lt;i&gt;Felgo&lt;/i&gt;, podjętego przez Łukasza Kosińskiego</translation>
    </message>
    <message>
        <location filename="../qml/scenes/AboutScene.qml" line="42"/>
        <source>VISIT_FELGO</source>
        <oldsource>Visit Felgo</oldsource>
        <translation>Odwiedź Felgo</translation>
    </message>
    <message>
        <location filename="../qml/scenes/AboutScene.qml" line="64"/>
        <source>VISIT_REPO</source>
        <oldsource>Visit repo</oldsource>
        <translation>Odwiedź repo</translation>
    </message>
    <message>
        <location filename="../qml/scenes/AboutScene.qml" line="88"/>
        <source>BACK</source>
        <oldsource>Back</oldsource>
        <translation>Wróć</translation>
    </message>
</context>
<context>
    <name>GameScene</name>
    <message>
        <location filename="../qml/scenes/GameScene.qml" line="162"/>
        <source>GAME_OVER</source>
        <translation>Żałosne</translation>
    </message>
</context>
<context>
    <name>MenuScene</name>
    <message>
        <location filename="../qml/scenes/MenuScene.qml" line="46"/>
        <source>START_GAME</source>
        <oldsource>Start Game</oldsource>
        <translation>Graj</translation>
    </message>
    <message>
        <location filename="../qml/scenes/MenuScene.qml" line="68"/>
        <source>SETTINGS</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../qml/scenes/MenuScene.qml" line="92"/>
        <source>ABOUT</source>
        <oldsource>About</oldsource>
        <translation>O grze</translation>
    </message>
</context>
<context>
    <name>SettingsScene</name>
    <message>
        <location filename="../qml/scenes/SettingsScene.qml" line="29"/>
        <source>LANGUAGE</source>
        <oldsource>Language</oldsource>
        <translation>Język</translation>
    </message>
    <message>
        <location filename="../qml/scenes/SettingsScene.qml" line="72"/>
        <source>MUSIC</source>
        <oldsource>Music</oldsource>
        <translation>Muzyka</translation>
    </message>
    <message>
        <location filename="../qml/scenes/SettingsScene.qml" line="96"/>
        <source>BACK</source>
        <oldsource>Back</oldsource>
        <translation>Wróć</translation>
    </message>
</context>
<context>
    <name>TranslationManager</name>
    <message>
        <location filename="../TranslationManager.cpp" line="68"/>
        <source>TARGET_LANGUAGE</source>
        <translation>Polski</translation>
    </message>
</context>
</TS>
