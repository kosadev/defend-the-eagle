import QtQuick 2.12
import Felgo 3.0
import "../entities"

Item {
    id: arena

    // use this to insert move anf fire actions
    property alias playerTank: playerTank

    // set size to specify arena size
    property int size: 480

    // arena contains 24 little areas by default
    readonly property int singleSquareSize: size / 24
    // tanks surface are twice bigger
    readonly property int mobSize: 2 * singleSquareSize
    // minimum size of margins to allow tanks to pass
    readonly property int minMargins: mobSize * 1.25

    // size of arena's borders
    readonly property int wallSize: 1

    width: size
    height: size

    // background of the level
    Rectangle {
        id: arenaBackroung
        anchors.fill: parent

        color: "#0d0d0d"
    }

    Tank {
        id: playerTank
        entityId: "playerTank"
        x: 300
        y: 200

        size: mobSize
    }


    Wall {
        id: bottomBorder

        height: wallSize
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
    }

    Wall {
        id: topBorder

        height: wallSize
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
    }

    Wall {
        id: leftBorder
        width: wallSize
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
        }
    }

    Wall {
        id: rightBorder
        width: wallSize
        anchors {
            top: parent.top
            bottom: parent.bottom
            right: parent.right
        }
    }


    Obstruction {
        id: topColumn1

        // margins are set to mobSize to allow tanks to pass
        anchors {
            top: parent.top
            topMargin: minMargins
            left: parent.left
            leftMargin: minMargins
        }

        width: singleSquareSize * 2
        height: size /2
    }

    Obstruction {
        id: topColumn2

        // margins are set to mobSize to allow tanks to pass
        anchors {
            top: parent.top
            topMargin: minMargins
            left: topColumn1.right
            leftMargin: minMargins
        }

        width: singleSquareSize * 2
        height: size /2
    }

    Obstruction {
        id: topColumn3

        // margins are set to mobSize to allow tanks to pass
        anchors {
            top: parent.top
            topMargin: minMargins
            left: topColumn2.right
            leftMargin: minMargins
        }

        width: singleSquareSize * 2
        height: size /2
    }

    Obstruction {
        id: topColumn4

        // margins are set to mobSize to allow tanks to pass
        anchors {
            top: parent.top
            topMargin: minMargins
            left: topColumn3.right
            leftMargin: minMargins
        }

        width: singleSquareSize * 2
        height: size /2
    }

    Obstruction {
        id: topColumn5

        // margins are set to mobSize to allow tanks to pass
        anchors {
            left: topColumn4.right
            leftMargin: minMargins
            verticalCenter: topColumn4.verticalCenter
        }

        width: singleSquareSize
        height: size/ 4
    }

    Obstruction {
        id: topRow

        // margins are set to mobSize to allow tanks to pass
        // left and right anchors to parent's borders allow to omit width attribute
        anchors {
            top: topColumn1.bottom
            topMargin: minMargins
            left: parent.left
            leftMargin: minMargins
            right: parent.right
            rightMargin: minMargins
        }

        // this row is gonna to be width
        height: singleSquareSize * 2
    }

    function handleInput(action) {
        playerTank.handleUserInput(action)
    }
}
