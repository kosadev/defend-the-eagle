import QtQuick 2.12
import Felgo 3.0
import "../input"

// Tank item could be controlled by user
EntityBase {
    id: tank
    entityType: "tank"

    signal tankDestroyed
    signal wallHitted

    // tank's speed
    property real forwardForce: 500 * world.pixelsPerMeter
    // tank's size
    property int size: 50

    // alias to access image source
    property alias tankImageSource: tankImage.source
    // alias to access axises controller
    property alias twoAxisController: twoAxisController

    Component.onCompleted: {
        var mapped = mapToItem(world.debugDraw, x, y)
    }

    Image {
        id: tankImage
        source: "../../assets/img/player-tank.png"
        anchors.fill: boxCollider

        // to rotate tank's barrel according to movement
        rotation: boxCollider.rotation

        // used to define initial position of bullet shot by tank
        Item {
            id: cordsPointerHorizontal
            x: tankImage.width / 2 + ((twoAxisController.xAxis == -1) ? - 1.8 * size : size/2)
        }
        Item {
            id: cordsPointerVertical
            y: tankImage.height / 2 + ((twoAxisController.yAxis == -1) ? - 1.8 * size : size/2)
        }
    }

    BoxCollider {
        id: boxCollider

        // the image and the physics will use size from global item property
        width: size
        height: size

        anchors.centerIn: parent

        density: 1
        friction: 0.6
        restitution: 0.15

        body.fixedRotation: true


        // tank moves forwards and backwards with the speed defined by forwardForce
        // speed is constant. Axises values are being set to -1, 0 or 1
        force: Qt.point(twoAxisController.xAxis * forwardForce, twoAxisController.yAxis * forwardForce)


        fixture.onBeginContact: {
            // get type of collider
            var fixture = other
            var body = other.getBody()
            var component = other.getBody().target
            var collidingType = component.entityType

            // tanks have only one life, if bullet reaches tank, then tank dies
            if(collidingType === "bullet") {
                destroySound.play()
                tank.tankDestroyed()
                tank.removeEntity()
            } else if(collidingType === "wall" | colliderType === "obstruction") {
                tank.wallHitted();
            }
        }

    }

    // twoAxisController is used to connect it's output with boxCollider movement
    TwoAxisController {
        id: twoAxisController

        // to point direction of tank to top from beginning
        yAxis: -1
        xAxis: 0
    }

    // fireSound is played after tank's shot
    SoundEffect {
        id: fireSound
        source: "../../assets/snd/fire.wav"
    }

    // destroySound is played just after enemy's bullet hits tank
    SoundEffect {
        id: destroySound
        source: "../../assets/snd/destroy.wav"
    }

    // user input is handled in this function. Function checks what to do:
    // move (in which direction?), stop or shot
    function handleUserInput(action) {
        switch(action) {
        case Input.InputAction.Fire:
            fire();
            break;
        case Input.InputAction.MoveUp:
            setAxises(0, -1)
            boxCollider.rotation = 0
            break;
        case Input.InputAction.MoveDown:
            setAxises(0, 1)
            boxCollider.rotation = 180
            break;
        case Input.InputAction.MoveLeft:
            setAxises(-1, 0)
            boxCollider.rotation = 270
            break;
        case Input.InputAction.MoveRight:
            setAxises(1, 0)
            boxCollider.rotation = 90
            break;
        case Input.InputAction.Stop:
            setAxises(0, 0)
            break;
        }
    }

    // to easily setup xAxis and yAxis parameters of twoAxisController
    // what is used in movement
    function setAxises(x, y) {
        twoAxisController.xAxis = x;
        twoAxisController.yAxis = y;

        // TO DO: inform Felgo that information about missing .outputXAxis parameter is still in repo
    }


    // used to perform shot
    function fire() {
        fireSound.play();

        // here we define beginning coordinates of the bullet
        var bulletCoordinates;
        if(twoAxisController.xAxis == 0) { // tank is positioned vertically
            bulletCoordinates = mapToItem(arena, cordsPointerVertical.x, cordsPointerVertical.y)
        } else { // tank is positioned horizontally
            bulletCoordinates = mapToItem(arena, cordsPointerHorizontal.x, cordsPointerHorizontal.y)
        }

        var bulletsTargetPoint = Qt.point(twoAxisController.xAxis, twoAxisController.yAxis)

        // bullet is craeted at location specified by tank position.
        // targetPoint is set to axises to shot bullet in same direction as tank was while it shot
        entityManager.createEntityFromUrlWithProperties(Qt.resolvedUrl("Bullet.qml"), {"x": bulletCoordinates.x, "y": bulletCoordinates.y, "targetPoint": bulletsTargetPoint})

    }
}
