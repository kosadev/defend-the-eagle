#ifndef TRANSLATION_MANAGER_H
#define TRANSLATION_MANAGER_H

#include <QObject>
#include <QTranslator>
#include <QQmlEngine>

// This simply TranslationManager class manages translations and installs translators.

class TranslationManager : public QObject
{
    Q_OBJECT

public:
    explicit TranslationManager(QQmlEngine *engine, QObject *parent = nullptr);
    ~TranslationManager();

    // Q_INVOKABLE decoration allows to access this method in qml. If translation is not available
    // then app will be translated to default language specified in private part of this class.
    Q_INVOKABLE void switchToLanguage(const QString &languageCode);

    // list translations availabe in resources. It returns map where
    // first element of each pair is language name and second one is language code
    QMap<QString, QString> avaliableTranslations() const;
    // checks if translation in among available translations
    bool isTranslationAvailable(const QString &languageCode) const;

private:
    QList<QTranslator*> _installedTranslators;

    QQmlEngine *_engine = nullptr;
    QString _currentLanguage = "en_US";
    const QString _defaultLanguage = "en_US";
};

#endif // TRANSLATION_MANAGER_H
