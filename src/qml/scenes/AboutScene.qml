import Felgo 3.0
import QtQuick 2.12

// aboutScene is a place where information about game and external links are displayed
GeneralMenuScene {
    id: aboutScene

    // emitted when backButton pressed in order to return to menu
    signal backPressed

    // let player be well informed about this game
    Text {
        id: gameDescription

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: visitFelgoButton.top
        anchors.bottomMargin: 50

        width: parent.width /2

        text: qsTr("ABOUT_GAME")
        horizontalAlignment: Text.AlignHCenter;

        color: "goldenrod"
        font.family: mainFont.family
        font.pixelSize: mainFont.pixelSize * 0.8
        font.weight: Font.Normal
    }

    // button to visit felgo.com
    StyledButton {
        id: visitFelgoButton
        // we want this button to be in app center
        anchors.centerIn: parent
        height: buttonHeight
        width: buttonWidth

        color: "#141313"
        Text {
            anchors.centerIn: parent
            text: qsTr("VISIT_FELGO")
            color: "white"
            font: mainFont
        }

        onClicked: {
            nativeUtils.openUrl("https://felgo.com");
        }
    }

    // button to visit repository's hostage
    StyledButton {
        id: visitRepoButton
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: visitFelgoButton.bottom
        anchors.topMargin: 20
        height: buttonHeight
        width: buttonWidth

        color: "#141313"
        Text {
            anchors.centerIn: parent
            text: qsTr("VISIT_REPO")
            color: "white"
            font: mainFont
        }

        onClicked: {
            nativeUtils.openUrl("https://gitlab.com/kosadev/defend-the-eagle/");
        }
    }


    // button to return to menu sccene
    StyledButton {
        id: backButtonButton
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: visitRepoButton.bottom
        anchors.topMargin: 20
        height: buttonHeight
        width: buttonWidth

        color: "#141313"

        Text {
            anchors.centerIn: parent
            text: qsTr("BACK")
            color: "white"
            font: mainFont
        }

        onClicked: {
            aboutScene.backPressed();
        }
    }
}
