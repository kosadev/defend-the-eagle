# allows to add DEPLOYMENTFOLDERS and links to the Felgo library and QtCreator auto-completion
CONFIG += felgo

# Project identifier and version
# More information: https://felgo.com/doc/felgo-publishing/#project-configuration
PRODUCT_IDENTIFIER = com.scythe.studiowizardEVP.DefendTheEagle
PRODUCT_VERSION_NAME = 1.0.0
PRODUCT_VERSION_CODE = 1

PRODUCT_LICENSE_KEY = ""

qmlFolder.source = qml
DEPLOYMENTFOLDERS += qmlFolder # comment for publishing

assetsFolder.source = assets
DEPLOYMENTFOLDERS += assetsFolder

# Add more folders to ship with the application here

TRANSLATIONS += \
    translations/en_US.ts \
    translations/de_DE.ts \
    translations/pl_PL.ts

RESOURCES += resources.qrc

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    TranslationManager.cpp

android {
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
    OTHER_FILES += android/AndroidManifest.xml       android/build.gradle
}

ios {
    QMAKE_INFO_PLIST = ios/Project-Info.plist
    OTHER_FILES += $$QMAKE_INFO_PLIST
}

# set application icons for win and macx
win32 {
    RC_FILE += win/app_icon.rc
}
macx {
    ICON = macx/app_icon.icns
}

DISTFILES += \
    qml/scenes/GameScene.qml \
    qml/input/MoveController.qml \
    qml/input/AimController.qml \
    qml/entities/Wall.qml \
    qml/entities/Tank.qml \
    qml/scenes/Arena.qml \
    qml/scenes/MenuScene.qml \
    qml/scenes/GeneralMenuScene.qml \
    qml/scenes/SettingsScene.qml \
    qml/entities/Bullet.qml \
    qml/entities/EnemyTank.qml

HEADERS += \
    TranslationManager.h
