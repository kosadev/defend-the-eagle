import Felgo 3.0
import QtQuick 2.0

// purpose of generalMenuScene is to keep other scenes as small as possible.
// In order to do that here are the code which could repeat in menu-like scene.
// We don't like repeated code.
Scene {
    id: generalMenuScene

    // to have same width in every buttons
    property int buttonWidth: width * 0.25
    // to have same height in every buttons
    property int buttonHeight: buttonWidth * 0.3


    // general mainFont unify font accros scene and lets us write less code
    property font mainFont: Qt.font({
        pixelSize: generalMenuScene.buttonHeight / 3,
        weight: Font.Medium,
        family: "Helvetica"
    })


    // background image for the menu
    Image {
        id: sceneBackground
        anchors.fill: parent
        source: "../../assets/img/metal.png"

        // to repeat texture
        fillMode: Image.Tile

        // to scale down texture
        sourceSize.width: 196
        sourceSize.height: 196

        // to hide background below other elements created earlier
        z: -1

        // to make background a bit darker we set this rectangle's opacity to 0.5
        Rectangle {
            anchors.fill: parent
            color: "black"
            opacity: 0.5
        }
    }
}
