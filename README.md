# Defend The Eagle
Purpose of this project is to create mobile game using Felgo SDK in order to pass challenge from Felgo Team. 

About
----
Defend The Eagle is inspired by famous retro game called Battle City. In this game you are located on the 2D area with walls and other obstractions. You control the tank and your goal is to protect the eagle against enemy tanks. 

However game is going to be modified for challenge's needs. There will be only one area level and game won't finish until player lose (it means lose tank). Time will say how good you are. 
Game difficulty increases with time which mean:
- more enemy tanks 
- different enemy's types * not implemented
- eagle is going to lose it's wall defence  * not implemented

Game is going to contain simple menu.

Links
----
Tutorial: https://docs.google.com/document/d/1ynmlGoyo93GqPMEeqxTo4Yxoy-rlhNc5XuIN4Nv3MV8/edit?usp=sharing

QDoc documentation: 

Original game: https://www.youtube.com/watch?v=X5VreNgScGo