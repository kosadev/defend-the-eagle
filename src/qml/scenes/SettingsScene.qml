import Felgo 3.0
import QtQuick 2.0

// in settingsScene player has opportunity to change language or mute/unmute music
GeneralMenuScene {
    id: settingsScene

    // emitted when changeLanguageButton clicked in order to retranslate app and change languageCode in settings
    signal languageChanged
    // emitted when muteButton clicked in order to mute/unmute menu music
    signal mutePressed
    // emitted when backButton clicked in order to return to menu
    signal backPressed

    // list of translations available in out app. It could be defined in top level
    readonly property var availableLanguages: ["en_US", "de_DE", "pl_PL"]
    // by default app's language is American English and exactly index of this one
    // is used to show user language flag
    property int currentLanguageIndex: 0

    // button to change game language
    StyledButton {
        id: changeLanguageButton
        // we want this button to be in app center
        anchors.centerIn: parent
        height: buttonHeight
        width: buttonWidth

        color: "#141313"
        Text {
            anchors.centerIn: parent
            text: qsTr("LANGUAGE")
            color: "white"
            font: mainFont
        }


        // flagIcon is an image to present flag suited to current language
        Image {
            id: flagIcon

            // currently showed flag is specified by currentLanguageIndex which referrs to
            // language code from array of available translations
            source: "../../assets/img/" + availableLanguages[currentLanguageIndex] + ".png"

            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 5

            height: parent.height / 2
            width: height
        }

        // when button is clicked, currentLanguageIndex increases
        onClicked: {
            ++currentLanguageIndex
            // if index is out of the party
            if(currentLanguageIndex >= availableLanguages.length) {
                currentLanguageIndex = 0
            }

            // signal is emitted in order to inform top level about this event
            settingsScene.languageChanged()
        }
    }

    // button to mute/unmute menu music
    StyledButton {
        id: muteButton
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: changeLanguageButton.bottom
        anchors.topMargin: 20
        height: buttonHeight
        width: buttonWidth

        color: "#141313"
        Text {
            anchors.centerIn: parent
            text: qsTr("MUSIC")
            color: "white"
            font: mainFont
        }

        onClicked: {
            // signal is emitted in order to inform top level about this event
            settingsScene.mutePressed();
        }
    }


    // button to return to menu sccene
    StyledButton {
        id: backButton
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: muteButton.bottom
        anchors.topMargin: 20
        height: buttonHeight
        width: buttonWidth

        color: "#141313"

        Text {
            anchors.centerIn: parent
            text: qsTr("BACK")
            color: "white"
            font: mainFont
        }

        onClicked: {
            settingsScene.backPressed();
        }
    }
}
