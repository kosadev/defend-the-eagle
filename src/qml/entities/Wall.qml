import Felgo 3.0
import QtQuick 2.0

// Wall is a entity used as Arena border
EntityBase {
    entityType: "wall"

    property int size: 50

    height: size
    width: size

    Rectangle {
        anchors.fill: collider
        color: "darkgray"
    }

    BoxCollider {
        id: collider
        anchors.fill: parent

        // walls should be solid. Nothing passes
        bodyType: Body.Static
    }
}
