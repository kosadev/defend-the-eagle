import Felgo 3.0
import QtQuick 2.12
import Qt.labs.settings 1.1
import "scenes"

GameWindow {
    id: gameWindow

    screenWidth: 960
    screenHeight: 640

    // from menuScene player can access game, settings and about scenes
    MenuScene {
      id: menuScene

      anchors.fill: parent

      // all scenes are transparent by default. This attribute is changed in states
      opacity: 0

      // handling signals defined in MenuScene item allows us to
      // change state of main GameWindows.
      onAboutPressed: gameWindow.state = "about"
      onOptionsPressed: gameWindow.state = "settings"
      onStartPressed: {
          gameWindow.state = "game"
          menuMusic.stop()

          // when scene is visible timerGenerator starts to generate enemies
          gameScene.timerGenerator.start()
          // create first enemy
          gameScene.generateEnemy()
      }
    }

    // aboutScene present game's description and external links
    AboutScene {
        id: aboutScene
        anchors.fill: parent
        opacity: 0

        onBackPressed: {
            gameWindow.state="menu"
        }
    }

    // main scene for game
    GameScene {
        id: gameScene;
        opacity: 0
    }

    // settingsScene allows user to change language and mute/unmute music in menu
    SettingsScene {
        id: settingsScene
        anchors.fill: parent

        opacity: 0

        // to go back form settings to menu
        onBackPressed: {
            gameWindow.state = "menu"
        }

        // function to change language in settings and whole app is called
        onLanguageChanged: {
            changeLanguage()
        }

        // handle mutePressed to adjust player's preferences and set muteMenuMusic in settings
        onMutePressed: {
            settings.muteMenuMusic = (settings.muteMenuMusic) ? false : true;
        }
    }


    // state of GameWindow is set to 'menu'.
    // It allows to have menuScene visible just after game start
    state: "menu"

    // this attribute allows us to define item's states which specify properties values
    // under different conditions
    states: [
        State {
            name: "menu"
            // by switching the opacity property to 1, which is by default set to 0 above,
            // the Behavior defined in SceneBase takes care of animating the opacity of the new Scene from 0 to 1,
            // and any other Scene back to its default value (in our case from 1 to 0)
            PropertyChanges { target: menuScene; opacity: 1}
        },
        State {
            name: "game"
            PropertyChanges { target: gameScene; opacity: 1}
        },
        State {
            name: "about"
            PropertyChanges { target: aboutScene; opacity: 1}
        },
        State {
            name: "settings"
            PropertyChanges { target: settingsScene; opacity: 1}
        }
    ]

    // settings contain two important properties which values stay even after reset
    Settings {
        id: settings

        // used to mute music in menu
        property bool muteMenuMusic: false
        // used to keep information about language selected by user
        property string currentLanguageCode: "en_US"

        // retranslate application if it's completed to load language saved in settings
        // index of current translation in settingsScene is set according to currentLanguageCode
        // propoerty from settings
        Component.onCompleted: {
            TranslationManager.switchToLanguage(currentLanguageCode)

            var availableCodes = settingsScene.availableLanguages

            for(var i = 0; i < availableCodes.length; ++i) {
                if(availableCodes[i] === settings.currentLanguageCode) {
                    settingsScene.currentLanguageIndex = i;
                }
            }
        }
    }

    // we really wish to have some cool tune in background
    // TO DO: inform Felgo about error
    BackgroundMusic {
        id: menuMusic
        source: "../assets/snd/epic-sax-guy.mp3"

        // muted parameter depends on settings
        muted: settings.muteMenuMusic
    }


    // function uses currently selected language code in settings scene language
    // then it's saved in settings and retranslating TranslatinManager::switchToLanguage()
    // is called
    function changeLanguage() {
        var availableCodes = settingsScene.availableLanguages
        var index = settingsScene.currentLanguageIndex

        var actualCode = availableCodes[index]

        settings.currentLanguageCode = actualCode;
        TranslationManager.switchToLanguage(actualCode) // call to C++ translate code
    }
}
