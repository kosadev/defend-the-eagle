import Felgo 3.0
import QtQuick 2.12

// menuScene is a first, default page showed to player. Game, Settings and About are accessed from here.
GeneralMenuScene {
    id: menuScene

    // emitted when startGameButton pressed in order to change scene to GameScene
    signal startPressed
    // emitted when optionsButton pressed in order to see game's options
    signal optionsPressed
    // emitted when aboutButton pressed in order to visit about page
    signal aboutPressed

    // let player know which game he plays
    Text {
        id: gameTitle

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: gameStartButton.top
        anchors.bottomMargin: 50

        // it is not translatable by purpose. Anyway game's name could
        // come from higher level like .pro file or C++, because it can change
        // on various project stages, so good practice is to keep such important
        // attribute like game's name in one place.
        text: "Defend The Eagle"

        color: "goldenrod"
        font.family: mainFont.family
        font.pixelSize: mainFont.pixelSize * 2.5
        font.weight: Font.DemiBold
    }

    // button to launch game
    StyledButton {
        id: gameStartButton
        // we want this button to be in app center
        anchors.centerIn: parent
        height: buttonHeight
        width: buttonWidth

        color: "#141313"
        Text {
            anchors.centerIn: parent
            text: qsTr("START_GAME")
            color: "white"
            font: mainFont
        }

        onClicked: {
            menuScene.startPressed()
        }
    }

    // button to open game's options scene
    StyledButton {
        id: optionsButton
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: gameStartButton.bottom
        anchors.topMargin: 20
        height: buttonHeight
        width: buttonWidth

        color: "#141313"
        Text {
            anchors.centerIn: parent
            text: qsTr("SETTINGS")
            color: "white"
            font: mainFont
        }

        onClicked: {
            menuScene.optionsPressed()
        }
    }


    // button to open game options scene
    StyledButton {
        id: aboutButton
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: optionsButton.bottom
        anchors.topMargin: 20
        height: buttonHeight
        width: buttonWidth

        color: "#141313"

        Text {
            anchors.centerIn: parent
            text: qsTr("ABOUT")
            color: "white"
            font: mainFont
        }

        onClicked: {
            menuScene.aboutPressed()
        }
    }

}
