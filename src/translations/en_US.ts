<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AboutScene</name>
    <message>
        <location filename="../qml/scenes/AboutScene.qml" line="22"/>
        <source>ABOUT_GAME</source>
        <translation>&lt;b&gt;Defend The Eagle&lt;/b&gt; was developed as a part of &lt;i&gt;Felgo Challenge&lt;/i&gt; for Łukasz Kosiński</translation>
    </message>
    <message>
        <location filename="../qml/scenes/AboutScene.qml" line="42"/>
        <source>VISIT_FELGO</source>
        <oldsource>Visit Felgo</oldsource>
        <translation>Visit Felgo</translation>
    </message>
    <message>
        <location filename="../qml/scenes/AboutScene.qml" line="64"/>
        <source>VISIT_REPO</source>
        <oldsource>Visit repo</oldsource>
        <translation>Visit repo</translation>
    </message>
    <message>
        <location filename="../qml/scenes/AboutScene.qml" line="88"/>
        <source>BACK</source>
        <oldsource>Back</oldsource>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>GameScene</name>
    <message>
        <location filename="../qml/scenes/GameScene.qml" line="162"/>
        <source>GAME_OVER</source>
        <translation>Game Over</translation>
    </message>
</context>
<context>
    <name>MenuScene</name>
    <message>
        <location filename="../qml/scenes/MenuScene.qml" line="46"/>
        <source>START_GAME</source>
        <oldsource>Start Game</oldsource>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../qml/scenes/MenuScene.qml" line="68"/>
        <source>SETTINGS</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../qml/scenes/MenuScene.qml" line="92"/>
        <source>ABOUT</source>
        <oldsource>About</oldsource>
        <translation>About</translation>
    </message>
</context>
<context>
    <name>SettingsScene</name>
    <message>
        <location filename="../qml/scenes/SettingsScene.qml" line="29"/>
        <source>LANGUAGE</source>
        <oldsource>Language</oldsource>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../qml/scenes/SettingsScene.qml" line="72"/>
        <source>MUSIC</source>
        <oldsource>Music</oldsource>
        <translation>Music</translation>
    </message>
    <message>
        <location filename="../qml/scenes/SettingsScene.qml" line="96"/>
        <source>BACK</source>
        <oldsource>Back</oldsource>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>TranslationManager</name>
    <message>
        <location filename="../TranslationManager.cpp" line="68"/>
        <source>TARGET_LANGUAGE</source>
        <translation>American English</translation>
    </message>
</context>
</TS>
